<?php
use Psr\Http\Message\RequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
use RingCentral\Psr7;
require __DIR__ . '/../vendor/autoload.php';
$loop = Factory::create();
    // prepare outgoing client request by updating request-target and Host header
    $host = (string)$request->getUri()->withScheme('')->withPath('')->withQuery('');
    $target = (string)$request->getUri()->withScheme('')->withHost('')->withPort(null);
    if ($target === '') {
        $target = $request->getMethod() === 'OPTIONS' ? '*' : '/';
    }
    $outgoing = $request->withRequestTarget($target)->withHeader('Host', $host);
        Psr7\str($outgoing)
$socket = new \React\Socket\Server(isset($argv[1]) ? $argv[1] : '192.168.43.140:10125', $loop);
$server->listen($socket);
echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;
$loop->run();
?>
