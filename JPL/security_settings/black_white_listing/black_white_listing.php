<?php 

$deny = array("192.168.43.20", "192.168.43.60", "192.168.43.75");
if (in_array ($_SERVER['REMOTE_ADDR'], $deny)) {
   header("location: https://192.168.43.125:10125/");
   exit();

} 

?>

<?php

function isAllowed($ip){
    $whitelist = array('192.168.43.70', '192.168.43.130', '192.168.43.54');

    // If the ip is matched, return true
    if(in_array($ip, $whitelist)) {
        return true;
    }

    foreach($whitelist as $i){
        $wildcardPos = strpos($i, "*");

        // Check if the ip has a wildcard
        if($wildcardPos !== false && substr($ip, 0, $wildcardPos) . "*" == $i) {
            return true;
        }
    }

    return false;
}

?>