<?php

use Aidantwoods\SecureHeaders\SecureHeaders;

$SecureHeaders = new SecureHeaders;
$SecureHeaders->strictMode();
$SecureHeaders->applyOnOutput();

setcookie('super-secret-token', base64_encode(random_bytes(12)));

$SecureHeaders->csp([
    'default' => 'none',
    'script' => [
        'https://www.google-analytics.com/analytics.js',
        'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/highlight.min.js',
    ],
    'style' => [
        'self',
        'https://fonts.googleapis.com/css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/styles/default.min.css',
    ],
    'font' => [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/',
        'https://fonts.gstatic.com/s/opensans/',
        'https://fonts.gstatic.com/s/sourcecodepro/',
    ],
    'image' => [
        'self',
        'https://www.google-analytics.com/',
    ],
    'base-uri' => 'self',
]);

$SecureHeaders->cspNonce('style');
$SecureHeaders->cspNonce('script');

?>