<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;

require __DIR__ . '/vendor/autoload.php';

$loop = Factory::create();

$server = new Server(function (ServerRequestInterface $request) {
    return new Response(
        200,
        array(
            'Content-Type' => 'text/html'
        ),
        "<html><head><title>Jupyter, PHP, Linux (JPL)</title></head><body><div align=center><iframe width=1000 height=700 src=http://192.168.43.150:10125/jupyter/noteb$
    );
});

$socket = new \React\Socket\Server('tls://192.168.43.135:10125', $loop, array(
        'tls' => array(
                'local_cert' => 'mycert.crt',
                'local_pk' => 'mycert.key',
                'verify_peer' => false,
                'verify_peer_name' => false
        )
));

$server->listen($socket);

//$socket->on('error', 'printf');

echo 'Listening on ' . str_replace('tls:', 'https:', $socket->getAddress()) . PHP_EOL;
$loop->run();

?>
